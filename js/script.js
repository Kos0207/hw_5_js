"use strict";
let userFirstName = prompt("Enter your firstname").trim();
let userLastName = prompt("Enter your lastname").trim();
while (userFirstName == null || !userFirstName) {
  userFirstName = prompt("Enter your firstname again");
}
while (userLastName == null || !userLastName) {
    userLastName = prompt("Enter your lastname again");
}
function createNewUser(userFirstName, userLastName) {
  const newUser = {
    firstName: userFirstName,
    lastName: userLastName,
    getLogin: () => {
      let login = userFirstName [0] + userLastName;
      return login.toLowerCase()
      
    },
    setFirstName: newFName => {
      Object.defineProperty(newUser, "firstName", {value: newFName})
    },
    setLastName: newLName => {
      Object.defineProperty(newUser, "lastName", {value: newLName})
    },
  };
  Object.defineProperty(newUser, "firstName", {value: userFirstName, configurable: true, writable: false});
  Object.defineProperty(newUser, "lastName", {value: userLastName, configurable: true, writable: false});
  return newUser;
}
const user = createNewUser(userFirstName, userLastName);
console.log("firstName: " + user.firstName + ", lastName: " + user.lastName);

console.log(user.getLogin()); 



